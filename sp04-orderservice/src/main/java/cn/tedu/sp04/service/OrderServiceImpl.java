package cn.tedu.sp04.service;

import cn.tedu.sp01.entity.Item;
import cn.tedu.sp01.entity.Order;
import cn.tedu.sp01.entity.User;
import cn.tedu.sp01.service.OrderService;
import cn.tedu.sp04.feign.ItemClient;
import cn.tedu.sp04.feign.UserClient;
import cn.tedu.web.util.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Slf4j
@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private ItemClient itemClient;

    @Autowired
    private UserClient userClient;



    @Override
    public Order getOrder(String orderId) {
        log.info("获取订单，orderId="+orderId);

        JsonResult<List<Item>> items = itemClient.getItems(orderId);
        JsonResult<User> user = userClient.getUser(8);//真实项目中，要获取已登录用户的id


        Order order = new Order();
        order.setId(orderId);
        order.setUser(user.getData());
        order.setItems(items.getData());
        return order;
    }

    @Override
    public void addOrder(Order order) {
        log.info("保存订单："+order);
        itemClient.decreaseNumber(order.getItems());
        userClient.addScore(order.getUser().getId(), 1000);
    }
}
