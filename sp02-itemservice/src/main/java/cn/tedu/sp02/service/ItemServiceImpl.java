package cn.tedu.sp02.service;

import cn.tedu.sp01.entity.Item;
import cn.tedu.sp01.service.ItemService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class ItemServiceImpl implements ItemService {

    @Override
    public List<Item> getItems(String orderId) {
        log.info("获取商品，orderId = "+orderId);
        List<Item> items = new ArrayList<>();
        items.add(new Item(1, "商品 1",1));
        items.add(new Item(2, "商品 2",2));
        items.add(new Item(3, "商品 3",3));
        items.add(new Item(4, "商品 4",4));
        items.add(new Item(5, "商品 5",5));
        return items;
    }

    @Override
    public void decreaseNumbers(List<Item> items) {
        for (Item item : items){
        log.info("减少库存 : "+item);
        }
    }
}
