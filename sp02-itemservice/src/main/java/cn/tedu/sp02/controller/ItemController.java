package cn.tedu.sp02.controller;

import cn.tedu.sp01.entity.Item;
import cn.tedu.sp01.service.ItemService;
import cn.tedu.web.util.JsonResult;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Random;

@RestController
@Slf4j
public class ItemController {
    @Autowired
    private ItemService itemService;

    @Value("${server.port}")
    private int port;

    @GetMapping("/{orderId}")
    public JsonResult<List<Item>> getItems(@PathVariable String orderId) throws InterruptedException {
        List<Item> items = itemService.getItems(orderId);

        if (Math.random() < 0.9) {
            //随机延迟时长，0到5秒
            int t = new Random().nextInt(5000);
            log.info("延迟： "+t);
            Thread.sleep(t);
        }

        return JsonResult.ok(items).msg("port = "+port);
    }

    @PostMapping("/decreaseNumber")
    public JsonResult<?> decreaseNumber(@RequestBody List<Item> items) {
        itemService.decreaseNumbers(items);
        return JsonResult.ok().msg("减少库存成功");
    }
    @GetMapping("/favicon.ico")
    public void ico() {
    }
}
