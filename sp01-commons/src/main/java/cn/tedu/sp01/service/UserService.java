package cn.tedu.sp01.service;

import cn.tedu.sp01.entity.User;

public interface UserService {
    User getUser(Integer id);
    void addScore(Integer id, Integer score);
}
