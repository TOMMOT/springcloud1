package cn.tedu.sp01.service;

import cn.tedu.sp01.entity.Item;

import java.util.List;

public interface ItemService {

    List<Item> getItems(String orderId);

    void decreaseNumbers(List<Item> list);
}
