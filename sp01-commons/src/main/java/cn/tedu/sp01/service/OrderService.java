package cn.tedu.sp01.service;

import cn.tedu.sp01.entity.Order;

public interface OrderService {
    Order getOrder(String orderId);
    void addOrder(Order order);
}
